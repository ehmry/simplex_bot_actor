# SimpleX bot actor

A [syndicated actor](https://syndicate-lang.org/) for collecting data from the the [SimpleX](https://simplex.chat/) websocket and reformatting as Syndicate assertions.
