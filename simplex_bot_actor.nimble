bin = @["simplex_bot_actor"]
license = "Unlicense"
srcDir = "src"
version = "20231028"

requires "nim", "syndicate"
