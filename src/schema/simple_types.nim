
import
  preserves, std/tables

type
  Contact* {.preservesRecord: "contact".} = object
    `field0`*: Table[Symbol, Preserve[void]]

  Command* {.preservesDictionary.} = object
    `cmd`*: string
    `corrId`*: string

  Chat* {.preservesRecord: "chat".} = object
    `field0`*: Table[Symbol, Preserve[void]]

  Group* {.preservesRecord: "group".} = object
    `field0`*: Table[Symbol, Preserve[void]]

proc `$`*(x: Contact | Command | Chat | Group): string =
  `$`(toPreserve(x))

proc encode*(x: Contact | Command | Chat | Group): seq[byte] =
  encode(toPreserve(x))
